/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : fgo_bot

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2015-08-12 20:33:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bots
-- ----------------------------
DROP TABLE IF EXISTS `bots`;
CREATE TABLE `bots` (
  `masterkey` varchar(255) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `authkey` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  `svt1` varchar(255) DEFAULT NULL,
  `svt2` varchar(255) DEFAULT NULL,
  `freestore` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`masterkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bots_10
-- ----------------------------
DROP TABLE IF EXISTS `bots_10`;
CREATE TABLE `bots_10` (
  `userid` varchar(255) NOT NULL,
  `s1` varchar(10) DEFAULT NULL,
  `s2` varchar(10) DEFAULT NULL,
  `s3` varchar(10) DEFAULT NULL,
  `s4` varchar(10) DEFAULT NULL,
  `s5` varchar(10) DEFAULT NULL,
  `s6` varchar(10) DEFAULT NULL,
  `s7` varchar(10) DEFAULT NULL,
  `s8` varchar(10) DEFAULT NULL,
  `s9` varchar(10) DEFAULT NULL,
  `s10` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bots_dsaber
-- ----------------------------
DROP TABLE IF EXISTS `bots_dsaber`;
CREATE TABLE `bots_dsaber` (
  `masterkey` varchar(255) DEFAULT NULL,
  `userid` varchar(255) NOT NULL,
  `authkey` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  `svt1` varchar(255) DEFAULT NULL,
  `svt2` varchar(255) DEFAULT NULL,
  `freestore` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bots_ip
-- ----------------------------
DROP TABLE IF EXISTS `bots_ip`;
CREATE TABLE `bots_ip` (
  `masterkey` varchar(255) DEFAULT NULL,
  `userid` varchar(255) NOT NULL,
  `authkey` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  `svt1` varchar(255) DEFAULT NULL,
  `svt2` varchar(255) DEFAULT NULL,
  `freestore` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
