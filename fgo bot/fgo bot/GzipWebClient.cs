﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot
{
    class GzipWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.KeepAlive = true;
            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            string cookie = response.Headers[HttpResponseHeader.SetCookie];
            if (cookie != null)
            {
                NetworkManager.cookie = cookie;
                Console.WriteLine(cookie);
            }
            return response;
        }
    }
}
