﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot
{
    class BattleManager
    {
        public static int battleId = 0;

        public static int activeDeckId = 0;

        public static int followId = 0;

        public static int svt = 0;

        public static int svt1 = 0;

        public static List<int> svtList = new List<int>();
        public static void setBattleIDByJson(string str)
        {
            JObject jo = JObject.Parse(str);
            battleId = jo["cache"]["replaced"]["battle"][0]["id"].Value<int>();
        }

        public static void setActivrDeckId(string str)
        {
            JObject jo = JObject.Parse(str);
            activeDeckId = jo["cache"]["updated"]["userGame"][0]["activeDeckId"].Value<int>();
        }

        public static void setFollowId(string str)
        {
            JObject jo = JObject.Parse(str);
            followId = jo["cache"]["updated"]["userFollower"][0]["followerInfo"][0]["userId"].Value<int>();
        }

        public static void setSvt(string str)
        {
            JObject jo = JObject.Parse(str);
            svt = jo["cache"]["replaced"]["battle"][0]["battleInfo"]["myDeck"]["svts"][0]["userSvtId"].Value<int>();
        }


        public static void setSvt1(string str)
        {
            JObject jo = JObject.Parse(str);
            svt1 = jo["response"][0]["success"]["gachaInfos"][0]["userSvtId"].Value<int>();
            svtList.Add(jo["response"][0]["success"]["gachaInfos"][0]["objectId"].Value<int>());
        }
    }
}
