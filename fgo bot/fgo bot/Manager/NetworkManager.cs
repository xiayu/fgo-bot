﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace fgo_bot
{

    class NetworkManager
    {
        public static string userId = null;

        public static string authKey = null;

        public static string secretKey = null;

        public static string userCreateServer = null;

        public static long serverOffsetTime = 1438853566;

        public static int saveIndex = 2;

        public static string cookie = null;

        public static string key = null;

        public static string datavar = "30";

        public static string appVer = "1.0.1";

        public static string action = "{ logs:[{\"uid\":1,\"ty\":2},{\"uid\":1,\"ty\":3},{\"uid\":1,\"ty\":5},{\"uid\":1,\"ty\":1},{\"uid\":1,\"ty\":1},{\"uid\":1,\"ty\":2},{\"uid\":1,\"ty\":2},{\"uid\":1,\"ty\":1},{\"uid\":1,\"ty\":3}]}";

        public static void SetUserInfo(AccountData data) 
        {
            userId = data.userId;
            authKey = data.authKey;
            secretKey = data.secretKey;
        }

        public static string GetServerUrl(bool flag = false)
        {
            if (flag)
            {
                return "http://" + userCreateServer;
            }
            return "http://" + userCreateServer;
        }

        public static string GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        protected static string getAuthFileName()
        {
            return "authsave" + saveIndex + ".dat";
        }

        public static void SetAuth(string userId, string authKey, string secretKey)
        {
            if (userId == null)
            {
                return;
            }
            NetworkManager.userId = userId;
            NetworkManager.authKey = authKey;
            NetworkManager.secretKey = secretKey;
        }

        public static void DeleteAuth()
        {
            string text = getAuthFileName();
            if (File.Exists(text))
            {
                File.Delete(text);
            }
        }

        public static void WriteAuth()
        {
            if (NetworkManager.userId == null)
            {
                return;
            }
            string authFileName = NetworkManager.getAuthFileName();
            using (BinaryWriter binaryWriter = new BinaryWriter(File.OpenWrite(authFileName)))
            {
                string value = CryptData.Encrypt(JsonManager.toJson(new Dictionary<string, object>
			{
				{
					"userCreateServer",
					NetworkManager.userCreateServer
				},
				{
					"userId",
					NetworkManager.userId
				},
				{
					"authKey",
					NetworkManager.authKey
				},
				{
					"secretKey",
					NetworkManager.secretKey
				}
			}));
                binaryWriter.Write(value);
            }
        }

        public static bool ReadAuth()
        {
            string text = getAuthFileName();
            if (!File.Exists(text))
            {
                if (!File.Exists(text))
                {
                    return false;
                }
            }
            using (BinaryReader binaryReader = new BinaryReader(File.OpenRead(text)))
            {
                string str = binaryReader.ReadString();
                string jsonstr = CryptData.Decrypt(str);
                Dictionary<string, object> dictionary = JsonManager.getDictionary(jsonstr);
                userCreateServer = dictionary["userCreateServer"].ToString();
                userId = dictionary["userId"].ToString();
                authKey = dictionary["authKey"].ToString();
                secretKey = dictionary["secretKey"].ToString();
                return true;
            }
        }

        public static void test(){
            userId = "14853093";
            authKey = "FGRiQoeCcOi9KORN:5aPiAAAAAAA=";
            secretKey = "L5NAlGsUflnZ2Nqm:5aPiAAAAAAA=";
        }

        public static NameValueCollection SetField(SortedDictionary<string, string> ex)
        {
            SortedDictionary<string, string> values = new SortedDictionary<string, string>();

            values.Add("userId", userId);
            values.Add("authKey", authKey);
            values.Add("appVer", appVer);
            values.Add("dataVer", datavar);
            values.Add("lastAccessTime", getTime());
            foreach (var item in ex)
            {
                values.Add(item.Key, item.Value);
            }
            values.Add("authCode", GetAuthCode(values));
            return ToNameValueCollection(values);
        }

        public static NameValueCollection SetBaseField(bool b = false)
        {
            SortedDictionary<string, string> values = new SortedDictionary<string, string>();

            if (b)
            {
                values.Add("userId", userId);
                values.Add("authKey", authKey);
            }

            values.Add("appVer", appVer);
            values.Add("dataVer", datavar);
            values.Add("lastAccessTime", "1442012416");
            values.Add("authCode", GetAuthCode(values));
            return ToNameValueCollection(values);
        }

        public static NameValueCollection ToNameValueCollection(
    SortedDictionary<string, string> dict)
        {
            var nameValueCollection = new NameValueCollection();

            foreach (var kvp in dict)
            {
                string value = null;
                if (kvp.Value != null)
                    value = kvp.Value.ToString();

                nameValueCollection.Add(kvp.Key.ToString(), value);
            }

            return nameValueCollection;
        }

        public static void setKey(string str) 
        {
            JObject jo = JObject.Parse(str);
            key = jo["cache"]["updated"]["userContinue"][0]["continueKey"].ToString();
            Console.WriteLine(key);
        }

        private static string getTime()
        {
            return GetTimeStamp();
        }

        public static string temp()
        {
            SortedDictionary<string, string> header = new SortedDictionary<string, string>();
            header.Add("userId", "7090985");
            header.Add("authKey", "GocqMr9/EeG1XrQk:KTNsAAAAAAA=");
            header.Add("appVer", "1.0.4");
            header.Add("lastAccessTime", "1442012416");
            header.Add("dataVer", "25");
            return GetAuthCode(header);
        }

        public static string GetAuthCode(SortedDictionary<string, string> headers)
        {
            string text = string.Empty;
            foreach (KeyValuePair<string, string> current in headers)
            {
                if (text != string.Empty)
                {
                    string text2 = text;
                    text = string.Concat(new string[]
				{
					text2,
					"&",
					current.Key,
					"=",
					current.Value
				});
                }
                else
                {
                    text = text + current.Key + "=" + current.Value;
                }
            }
            if (NetworkManager.secretKey == null)
            {
                return null;
            }
            SHA1 sHA = new SHA1CryptoServiceProvider();
            UTF8Encoding uTF8Encoding = new UTF8Encoding();
            byte[] bytes = uTF8Encoding.GetBytes(text + ":" + NetworkManager.secretKey);
            byte[] inArray = sHA.ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }
    }
}
