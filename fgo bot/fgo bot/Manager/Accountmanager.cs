﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot
{
    class AccountData 
    {
        public string ContinueCode;
        public string userId;
        public string authKey;
        public string secretKey;
        public string freestore;
    }

    class Accountmanager
    {
        public List<AccountData> datas = new List<AccountData>();

        private static Accountmanager _instance;

        public static Accountmanager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Accountmanager();
            }
            return _instance;
        }

        private Accountmanager()
        {

        }

        public void getAllData(MySqlCommand mySqlCommand)
        {
            datas.Clear();
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                if (reader.HasRows)
                {
                    AccountData data = new AccountData();
                    data.ContinueCode = reader.GetString(0);
                    data.userId = reader.GetString(1);
                    data.authKey = reader.GetString(2);
                    data.secretKey = reader.GetString(3);
                    data.freestore = reader.GetString("freestore");
                    datas.Add(data);
                    //Console.WriteLine(data.ContinueCode + " " + data.userId + " " + data.authKey + " " + data.secretKey);
                }
            }
            reader.Close();
            mySqlCommand.Dispose();
        }
    }
}
