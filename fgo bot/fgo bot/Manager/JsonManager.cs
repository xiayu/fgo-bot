﻿using MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace fgo_bot
{
    public class JsonManager
    {
        private static JsonManager singleton = new JsonManager();

        private static StringBuilder strBuilder;

        private JsonManager()
        {
        }

        public static JsonManager GetInstance()
        {
            return JsonManager.singleton;
        }

        public static Dictionary<string, object> getDictionary(string jsonstr)
        {
            Dictionary<string, object> dictionary = (Dictionary<string, object>)Json.Deserialize(jsonstr);
            if (dictionary == null)
            {
            }
            return dictionary;
        }

        public static Dictionary<string, object> getDictionary(object obj)
        {
            return (Dictionary<string, object>)obj;
        }

        public static T Deserialize<T>(object obj)
        {
            return JsonManager.CreateByDict<T>(obj);
        }

        public static T[] DeserializeArray<T>(object obj)
        {
            return JsonManager.CreateByDictArray<T>(obj);
        }

        private static void CreateJsonString(object obj)
        {
            if (obj == null)
            {
                JsonManager.strBuilder.Append(" null ,");
            }
            else if (obj.GetType() == typeof(Dictionary<string, object>))
            {
                JsonManager.strBuilder.Append(Json.Serialize(obj));
            }
            else if (obj.GetType() == typeof(List<object>))
            {
                JsonManager.strBuilder.Append("[");
                bool flag = false;
                foreach (object current in ((List<object>)obj))
                {
                    if (flag)
                    {
                        JsonManager.strBuilder.Append(",");
                    }
                    else
                    {
                        flag = true;
                    }
                    JsonManager.CreateJsonString(current);
                }
                JsonManager.strBuilder.Append("]");
            }
            else if (obj.GetType() == typeof(int))
            {
                JsonManager.strBuilder.Append(((int)obj).ToString());
            }
            else if (obj.GetType() == typeof(long))
            {
                JsonManager.strBuilder.Append(((long)obj).ToString());
            }
            else if (obj.GetType() == typeof(bool))
            {
                JsonManager.strBuilder.Append((!(bool)obj) ? "false" : "true");
            }
            else if (obj.GetType() == typeof(string))
            {
                JsonManager.strBuilder.Append("\"" + (string)obj + "\"");
            }
            else if (obj.GetType() == typeof(float))
            {
                JsonManager.strBuilder.Append(((float)obj).ToString());
            }
            else if (obj.GetType() == typeof(double))
            {
                JsonManager.strBuilder.Append(((double)obj).ToString());
            }
            else if (obj.GetType().IsArray)
            {
                Type elementType = obj.GetType().GetElementType();
                if (elementType == typeof(int) || elementType == typeof(long) || elementType == typeof(bool) || elementType == typeof(string) || elementType == typeof(float) || elementType == typeof(double))
                {
                    JsonManager.strBuilder.Append(Json.Serialize(obj));
                }
                else
                {
                    JsonManager.strBuilder.Append("[");
                    Array array = (Array)obj;
                    bool flag2 = false;
                    foreach (object current2 in array)
                    {
                        if (flag2)
                        {
                            JsonManager.strBuilder.Append(",");
                        }
                        else
                        {
                            flag2 = true;
                        }
                        JsonManager.CreateJsonString(current2);
                    }
                    JsonManager.strBuilder.Append("]");
                }
            }
            else
            {
                JsonManager.strBuilder.Append("{");
                FieldInfo[] fields = obj.GetType().GetFields();
                bool flag3 = false;
                FieldInfo[] array2 = fields;
                for (int i = 0; i < array2.Length; i++)
                {
                    FieldInfo fieldInfo = array2[i];
                    if (flag3)
                    {
                        JsonManager.strBuilder.Append(",");
                    }
                    else
                    {
                        flag3 = true;
                    }
                    JsonManager.strBuilder.Append("\"" + fieldInfo.Name + "\":");
                    JsonManager.CreateJsonString(fieldInfo.GetValue(obj));
                }
                JsonManager.strBuilder.Append("}");
            }
        }

        public static string toJson(object obj)
        {
            JsonManager.strBuilder = new StringBuilder();
            JsonManager.CreateJsonString(obj);
            return JsonManager.strBuilder.ToString();
        }

        private static void CreateByDictInside(object dat, Type t, Dictionary<string, object> dict)
        {
            if (dict == null)
            {
                return;
            }
            foreach (KeyValuePair<string, object> current in dict)
            {
                FieldInfo field = t.GetField(current.Key);
                object value = current.Value;
                if (field != null)
                {
                    if (field.FieldType == typeof(int))
                    {
                        long num = (long)value;
                        field.SetValue(dat, (int)num);
                    }
                    else if (field.FieldType == typeof(long))
                    {
                        field.SetValue(dat, (long)value);
                    }
                    else if (field.FieldType == typeof(bool))
                    {
                        field.SetValue(dat, (bool)value);
                    }
                    else if (field.FieldType == typeof(string))
                    {
                        field.SetValue(dat, (string)value);
                    }
                    else if (field.FieldType == typeof(float))
                    {
                        if (value.GetType() == typeof(long) || value.GetType() == typeof(int))
                        {
                            field.SetValue(dat, (int)((long)value));
                        }
                        else
                        {
                            field.SetValue(dat, (float)((double)value));
                        }
                    }
                    else if (field.FieldType == typeof(double))
                    {
                        field.SetValue(dat, (double)value);
                    }
                    else if (field.FieldType.IsArray)
                    {
                        if (field.FieldType.GetElementType() == typeof(int))
                        {
                            if (value != null)
                            {
                                int[] array = new int[((IList)value).Count];
                                for (int i = 0; i < array.Length; i++)
                                {
                                    long num2 = (long)((IList)value)[i];
                                    array[i] = (int)num2;
                                }
                                field.SetValue(dat, array);
                            }
                        }
                        else if (field.FieldType.GetElementType() == typeof(long))
                        {
                            if (value != null)
                            {
                                long[] array2 = new long[((IList)value).Count];
                                for (int j = 0; j < array2.Length; j++)
                                {
                                    long num3 = (long)((IList)value)[j];
                                    array2[j] = num3;
                                }
                                field.SetValue(dat, array2);
                            }
                        }
                        else if (field.FieldType.GetElementType() == typeof(double))
                        {
                            if (value != null)
                            {
                                double[] array3 = new double[((IList)value).Count];
                                for (int k = 0; k < array3.Length; k++)
                                {
                                    double num4 = (double)((IList)value)[k];
                                    array3[k] = num4;
                                }
                                field.SetValue(dat, array3);
                            }
                        }
                        else if (field.FieldType.GetElementType() == typeof(string))
                        {
                            string[] array4 = new string[((IList)value).Count];
                            for (int l = 0; l < array4.Length; l++)
                            {
                                array4[l] = (string)((IList)value)[l];
                            }
                            field.SetValue(dat, array4);
                        }
                        else
                        {
                            Array array5 = Array.CreateInstance(field.FieldType.GetElementType(), ((IList)value).Count);
                            for (int m = 0; m < ((object[])array5).Length; m++)
                            {
                                ((object[])array5)[m] = Activator.CreateInstance(field.FieldType.GetElementType());
                                JsonManager.CreateByDictInside(((object[])array5)[m], field.FieldType.GetElementType(), (Dictionary<string, object>)((IList)value)[m]);
                            }
                            field.SetValue(dat, array5);
                        }
                    }
                    else if (field.FieldType == typeof(Dictionary<string, object>))
                    {
                        object obj = current.Value;
                        if (obj.GetType() == typeof(string))
                        {
                            obj = Json.Deserialize((string)obj);
                        }
                        field.SetValue(dat, obj);
                    }
                    else
                    {
                        object obj2 = Activator.CreateInstance(field.FieldType);
                        JsonManager.CreateByDictInside(obj2, field.FieldType, (Dictionary<string, object>)value);
                        field.SetValue(dat, obj2);
                    }
                }
                else if (dat.GetType() == typeof(Dictionary<string, object>))
                {
                    Dictionary<string, object> dictionary = (Dictionary<string, object>)dat;
                    dictionary[current.Key] = current.Value;
                }
            }
        }

        private static T CreateByDict<T>(object obj)
        {
            if (typeof(T).IsArray)
            {
                if (obj.GetType() == typeof(string))
                {
                    obj = Json.Deserialize((string)obj);
                }
                int count = ((IList)obj).Count;
                Type elementType = typeof(T).GetElementType();
                object obj2 = Activator.CreateInstance(typeof(T), new object[]
			{
				count
			});
                for (int i = 0; i < count; i++)
                {
                    ((object[])obj2)[i] = Activator.CreateInstance(elementType);
                    JsonManager.CreateByDictInside(((object[])obj2)[i], elementType, (Dictionary<string, object>)((IList)obj)[i]);
                }
                return (T)((object)obj2);
            }
            if (typeof(T) == typeof(Dictionary<string, object>))
            {
                object obj3 = Activator.CreateInstance(typeof(Dictionary<string, object>));
                JsonManager.CreateByDictInside(obj3, typeof(T), (Dictionary<string, object>)obj);
                return (T)((object)obj3);
            }
            if (obj.GetType() == typeof(string))
            {
                obj = Json.Deserialize((string)obj);
            }
            T t = Activator.CreateInstance<T>();
            JsonManager.CreateByDictInside(t, typeof(T), (Dictionary<string, object>)obj);
            return t;
        }

        private static T[] CreateByDictArray<T>(object obj)
        {
            if (obj.GetType() == typeof(string))
            {
                obj = Json.Deserialize((string)obj);
            }
            int count = ((IList)obj).Count;
            T[] array = new T[count];
            if (typeof(T) == typeof(object))
            {
                for (int i = 0; i < count; i++)
                {
                    array[i] = Activator.CreateInstance<T>();
                    JsonManager.CreateByDictInside(array[i], typeof(T), (Dictionary<string, object>)((IList)obj)[i]);
                }
            }
            else
            {
                for (int j = 0; j < count; j++)
                {
                    array[j] = JsonManager.Deserialize<T>(((IList)obj)[j]);
                }
            }
            return array;
        }
    }
}
