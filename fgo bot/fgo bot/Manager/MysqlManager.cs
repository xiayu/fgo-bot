﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace fgo_bot
{
    class MysqlManager
    {
        MySqlConnection myCon = null;
        static MysqlManager _instance = null;
        public static MysqlManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new MysqlManager();
            }
            return _instance;
        }

        private MysqlManager()
        {
            if (myCon == null)
            {
                myCon = getMysqlConnection();
            }
        }

        private MySqlConnection getMysqlConnection()
        {
            string M_str_sqlcon = "server=127.0.0.1;port=3306;userid=root;password=;database=fgo_bot";
            myCon = new MySqlConnection(M_str_sqlcon);
            myCon.Open();
            return myCon;
        }

        public MySqlCommand execReader(string sql)
        {
            Console.WriteLine(sql);
            MySqlCommand mysqlcom = new MySqlCommand(sql, myCon);
            return mysqlcom;
        }

        public void exec(string str)
        {
            Console.WriteLine(str);
            MySqlCommand mysqlcom = new MySqlCommand(str, myCon);
            mysqlcom.ExecuteNonQuery();
            mysqlcom.Dispose();
        }

        public void exec(string key, string stv1, string stv2)
        {
            string str = "INSERT INTO bots_ip VALUES('" + key + "'," + NetworkManager.userId + ",'" + NetworkManager.authKey + "','" + NetworkManager.secretKey + "'," + stv1 + "," + stv2 + ", 0)";
            Console.WriteLine(str);
            MySqlCommand mysqlcom = new MySqlCommand(str, myCon);
            mysqlcom.ExecuteNonQuery();
            mysqlcom.Dispose();
        }

        public void Release()
        {
            if (myCon != null)
            {
                myCon.Close();
                myCon.Dispose();
            }
        }
    }
}
