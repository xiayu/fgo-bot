﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fgo_bot.Request;
using fgo_bot.DebugCase;
using System.Threading;
using System.Net;

namespace fgo_bot
{
    enum RunCase
    {
        RegistBot,
        DailyLogin,
        TenGacha,
        MasterFileToJson,
        MysqlTest,
        CalcMysql,
        RegistDropBot,
        RunDrop,
        CheckAccount,
        AddFriend,
        FP,
        FPLoop,
        UnKnow
    }

    class Program
    {
        public static bool DebugMode = false;

        static RunCase runCase = RunCase.FPLoop;
        static void Main(string[] args)
        {
            Console.WriteLine(NetworkManager.GetTimeStamp());

            BaseCase testCase;
            switch (runCase)
            {
                    
                case RunCase.RegistBot:
                    testCase = new RegistAndContinueCase(1);
                    testCase.run();
                    break;
                case RunCase.RegistDropBot:
                    testCase = new RegistDropBotsCase(100);
                    testCase.run();
                    break;
                case RunCase.DailyLogin:
                    testCase = new DailyLoginCase();
                    testCase.run();
                    break;
                case RunCase.TenGacha:
                    testCase = new TenGachaCase(1000);
                    testCase.run();
                    break;
                case RunCase.MysqlTest:
                    testCase = new MysqlSelectTestCase();
                    testCase.run();
                    break;
                case RunCase.MasterFileToJson:
                    MasterFile.ReadMasterData();
                    break;
                case RunCase.CalcMysql:
                    testCase = new CalcMysql();
                    testCase.run();
                    break;
                case RunCase.CheckAccount:
                    testCase = new CheckAccountCase();
                    testCase.run();
                    break;
                case RunCase.AddFriend:
                    testCase = new AddFriendsCase();
                    testCase.run();
                    break;
                case RunCase.FPLoop:
                    testCase = new AddFriendsCase();
                    for (; ; )
                    {
                        Thread.Sleep(1000 * 60 * 92);
                        testCase.run();
                    }
                case RunCase.FP:
                    testCase = new FPCase();
                    testCase.run();
                    break;
                default:
                    NetworkManager.ReadAuth();
                    NetworkManager.test();
                    Console.WriteLine(NetworkManager.temp());
                    TopLoginRequest loginRequest = new TopLoginRequest();
                    loginRequest.StartWebClient();
                    break;
            }
            Console.WriteLine(NetworkManager.GetTimeStamp());
        }
    }
}
