﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot
{
    public class CryptData
    {
        private const string CKEY = "b5nHjsMrqaeNliSs3jyOzgpD";

        private const string CVEC = "wuD6keVr";

        public static string Encrypt(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            byte[] bytes2 = Encoding.UTF8.GetBytes("b5nHjsMrqaeNliSs3jyOzgpD");
            byte[] bytes3 = Encoding.UTF8.GetBytes("wuD6keVr");
            TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESCryptoServiceProvider.CreateEncryptor(bytes2, bytes3), CryptoStreamMode.Write);
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.Close();
            byte[] inArray = memoryStream.ToArray();
            memoryStream.Close();
            return Convert.ToBase64String(inArray);
        }

        public static string Decrypt(string str)
        {
            byte[] array = Convert.FromBase64String(str);
            byte[] bytes = Encoding.UTF8.GetBytes("b5nHjsMrqaeNliSs3jyOzgpD");
            byte[] bytes2 = Encoding.UTF8.GetBytes("wuD6keVr");
            TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESCryptoServiceProvider.CreateDecryptor(bytes, bytes2), CryptoStreamMode.Write);
            cryptoStream.Write(array, 0, array.Length);
            cryptoStream.Close();
            byte[] bytes3 = memoryStream.ToArray();
            memoryStream.Close();
            return Encoding.UTF8.GetString(bytes3);
        }

    }
}
