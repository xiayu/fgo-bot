﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;

namespace fgo_bot
{
    class Svt
    {
        public int id;
        public string name;
    }
    class SvtDatabase
    {
        public static Dictionary<int, Svt> database = new Dictionary<int, Svt>();

        public static void init(string path)
        {
            if (!File.Exists(path))
            {
                return;
            }
            using (StreamReader sm = new StreamReader(File.OpenRead(path)))
            {
                string jsonstr = sm.ReadToEnd();
                JArray jArray = JArray.Parse(jsonstr);
                foreach (JObject item in jArray)
                {
                    Svt s = new Svt();
                    s.id = item["id"].Value<int>();
                    s.name = item["name"].ToString();
                    database.Add(s.id, s);
                }
            }
        }

    }
}
