﻿using fgo_bot.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace fgo_bot.DebugCase
{
    class RegistAndContinueCase:BaseCase
    {
        int num;

        public RegistAndContinueCase(int n)
        {
            num = n;
        }

        private void runCommond()
        {
            TopGameDataRequest topRequest = new TopGameDataRequest();
            topRequest.StartWebClient();
            AccountRegistRequest accountRequest = new AccountRegistRequest();
            accountRequest.StartWebClient();
            TopSignUpRequest signupRequest = new TopSignUpRequest();
            signupRequest.StartWebClient();
            TopLoginRequest loginRequest = new TopLoginRequest();
            loginRequest.StartWebClient();
            BattleSetupRequest setupRequest = new BattleSetupRequest();
            setupRequest.StartWebClient(1000000, 1, 0, 0);
            BattleResultRequest resultRequest = new BattleResultRequest();
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            ProfileEditNameRequest nameRequest = new ProfileEditNameRequest();
            nameRequest.StartWebClient(Util.GenerateCheckCode(5), 2);
            setupRequest.StartWebClient(1000001, 1, 0, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(1000002, 1, 0, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            TutorialSetRequest tutorialRequest = new TutorialSetRequest();
            tutorialRequest.StartWebClient();
            GaChaRequest gachaRequest = new GaChaRequest();
            gachaRequest.StartWebClient(101, 1, 0, 1);
            DeckSetupRequest deckRequest = new DeckSetupRequest();
            deckRequest.StartWebClient(BattleManager.activeDeckId, BattleManager.activeDeckId);
            setupRequest.StartWebClient(1000003, 1, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(93000001, 1, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(93000001, 2, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(93000001, 3, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(1000003, 2, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(1000003, 3, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            gachaRequest.StartWebClient(21001, 1, 0, 1);
            ContinuePrepareRequest continueRequest = new ContinuePrepareRequest();
            continueRequest.StartWebClient();
        }

        public override void run()
        {
            NetworkManager.ReadAuth();
            //MysqlManager m = MysqlManager.GetInstance();

            for (int i = 0; i < num; i++)
            {
                try
                {
                    runCommond();
                    //m.exec(NetworkManager.key, BattleManager.svtList[0].ToString(), BattleManager.svtList[1].ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
            }
        }
    }
}
