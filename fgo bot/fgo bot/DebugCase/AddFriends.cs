﻿using fgo_bot.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace fgo_bot.DebugCase
{
    class AddFriendsCase :BaseCase
    {
        public override void run()
        {
            MysqlManager manager = MysqlManager.GetInstance();
            Accountmanager am = Accountmanager.GetInstance();
            am.getAllData(manager.execReader("select * from fp_bots limit 10"));
            NetworkManager.ReadAuth();
            for (int i = 0; i < am.datas.Count; i++)
            {
                try
                {
                    AccountData data = am.datas[i];
                    MysqlManager mm = MysqlManager.GetInstance();
                    NetworkManager.SetUserInfo(data);
                    TopGameDataRequest topRequest = new TopGameDataRequest();
                    topRequest.StartWebClient();
                    Thread.Sleep(1000);
                    TopLoginRequest loginRequest = new TopLoginRequest();
                    loginRequest.StartWebClient();
                    AddFriendRequest addFriendRequest = new AddFriendRequest();
                    addFriendRequest.StartWebClient();
                }
                catch (Exception e)
                {
                    Console.WriteLine("net work error" + e.ToString());
                    continue;
                }
            }
        }
    }
}
