﻿using fgo_bot.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace fgo_bot.DebugCase
{
    class TenGachaCase :BaseCase
    {
        int num;

        List<int> svts = new List<int>();

        public TenGachaCase(int n)
        {
            num = n;
        }

        private void addToList(string str)
        {
            JObject jo = JObject.Parse(str);
            JArray ja = jo["response"][0]["success"]["gachaInfos"].Value<JArray>();
            foreach (JObject item in ja)
            {
                svts.Add(item["objectId"].Value<int>());
            }
        }

        public override void run()
        {
            MysqlManager manager = MysqlManager.GetInstance();
            NetworkManager.ReadAuth();
            Accountmanager am = Accountmanager.GetInstance();
            am.getAllData(manager.execReader("select * from bots where userid > 0 and freestore>=40 limit "+num));
                foreach (AccountData data in am.datas)
                {
                    svts.Clear();
                    MysqlManager mm = MysqlManager.GetInstance();
                    NetworkManager.SetUserInfo(data);
                    TopGameDataRequest topRequest = new TopGameDataRequest();
                    topRequest.StartWebClient();
                    TopSignUpRequest signupRequest = new TopSignUpRequest();
                    signupRequest.StartWebClient();
                    TopLoginRequest loginRequest = new TopLoginRequest();
                    loginRequest.StartWebClient();
                    GaChaRequest tenGachaRequest = new GaChaRequest();
                    string temp = tenGachaRequest.StartWebClient(21001, 10, 0, 2);
                    addToList(temp);
                    temp = string.Join(",", svts);
                    mm.exec("INSERT INTO bots_10 VALUES(" + data.userId + " ," + temp + ")");
                    data.freestore = Int32.Parse(data.freestore) - 40 + "";
                    mm.exec("UPDATE bots SET freestore = " + data.freestore + " WHERE userid = " + data.userId + " ");
                }
        }
    }
}
