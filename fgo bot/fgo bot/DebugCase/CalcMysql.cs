﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fgo_bot.DebugCase
{
    class CalcMysql:BaseCase
    {
        public override void run()
        {
            SvtDatabase.init(@"MasterDataCaches\mstSvt.dat.json");
            Dictionary<string, int> dict = new Dictionary<string, int>();
            MysqlManager mm = MysqlManager.GetInstance();
            var mySqlCommand = mm.execReader("select * from bots_10");
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                if (reader.HasRows)
                {
                    int result = 0;
                    for (int i = 1; i < 11; i++)
                    {
                        int num = reader.GetInt32(i);
                        if (num == 100100 || num == 101800 || num == 501900 || num == 700700 || num == 900100)
                        {
                            result++;
                        }
                    }
                    if (result > 1)
                    {
                        for (int i = 1; i < 11; i++)
                        {
                            int num = reader.GetInt32(i);
                            if (num == 100100 || num == 101800 || num == 501900 || num == 700700 || num == 900100)
                            {
                                Console.Write(num + " ");
                            }
                        }
                        Console.WriteLine();
                    }
                }
            }
            foreach (var item in dict)
            {
                Console.WriteLine(item.Key + ":" + item.Value);
            }
            using (StreamWriter sw = new StreamWriter("text.txt"))
            {
                foreach (var item in dict)
                {
                    sw.WriteLine(item.Key + ":" + item.Value);
                }
            }
            reader.Close();
            mySqlCommand.Dispose();
        }
    }
}
