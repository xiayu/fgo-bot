﻿using fgo_bot.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace fgo_bot.DebugCase
{
    class RegistDropBotsCase :BaseCase
    {
        int num;
        public RegistDropBotsCase(int n)
        {
            num = n;
        }

        private void runCommond()
        {
            TopGameDataRequest topRequest = new TopGameDataRequest();
            topRequest.StartWebClient();
            AccountRegistRequest accountRequest = new AccountRegistRequest();
            accountRequest.StartWebClient();
            TopSignUpRequest signupRequest = new TopSignUpRequest();
            signupRequest.StartWebClient();
            TopLoginRequest loginRequest = new TopLoginRequest();
            loginRequest.StartWebClient();
            Thread.Sleep(10000);
            BattleSetupRequest setupRequest = new BattleSetupRequest();
            setupRequest.StartWebClient(1000000, 1, 0, 0);
            BattleResultRequest resultRequest = new BattleResultRequest();
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            ProfileEditNameRequest nameRequest = new ProfileEditNameRequest();
            nameRequest.StartWebClient(Util.GenerateCheckCode(5), 2);
            setupRequest.StartWebClient(1000001, 1, 0, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(1000002, 1, 0, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            Thread.Sleep(1000);
            TutorialSetRequest tutorialRequest = new TutorialSetRequest();
            tutorialRequest.StartWebClient();
            GaChaRequest gachaRequest = new GaChaRequest();
            gachaRequest.StartWebClient(101, 1, 0, 1);
            DeckSetupRequest deckRequest = new DeckSetupRequest();
            deckRequest.StartWebClient(BattleManager.activeDeckId, BattleManager.activeDeckId);
            setupRequest.StartWebClient(1000003, 1, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(93000001, 1, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(93000001, 2, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(93000001, 3, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(1000003, 2, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            setupRequest.StartWebClient(1000003, 3, BattleManager.activeDeckId, 0);
            resultRequest.StartWebClient(BattleManager.battleId, 1, "", NetworkManager.action);
            Thread.Sleep(10000);
            ContinuePrepareRequest continueRequest = new ContinuePrepareRequest();
            continueRequest.StartWebClient();
        }

        public override void run()
        {
            NetworkManager.ReadAuth();
            MysqlManager m = MysqlManager.GetInstance();

            for (int i = 0; i < num; i++)
            {
                try
                {
                    runCommond();
                    string str = "INSERT INTO bots_drop VALUES('" + NetworkManager.key + "'," + NetworkManager.userId + ",'" + NetworkManager.authKey + "','" + NetworkManager.secretKey + "'," + BattleManager.svtList[0].ToString() + ", 0)";
                    Console.WriteLine(str);
                    m.exec(str);
                    Thread.Sleep(30000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
            }
        }
    }
}
