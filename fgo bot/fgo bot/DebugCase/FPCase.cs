﻿using fgo_bot.Request;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace fgo_bot.DebugCase
{
    class FPCase :BaseCase
    {
        int num = 6;
        public override void run()
        {
            MysqlManager manager = MysqlManager.GetInstance();
            Accountmanager am = Accountmanager.GetInstance();
            am.getAllData(manager.execReader("select * from fp_bots limit 10"));
            NetworkManager.ReadAuth();
            for (int i = 0; i < am.datas.Count; i++)
            {
                try
                {
                    AccountData data = am.datas[i];
                    MysqlManager mm = MysqlManager.GetInstance();
                    NetworkManager.SetUserInfo(data);
                    TopGameDataRequest topRequest = new TopGameDataRequest();
                    topRequest.StartWebClient();
                    Thread.Sleep(1000);
                    TopLoginRequest loginRequest = new TopLoginRequest();
                    loginRequest.StartWebClient();
                    FollowListRequest followListRequest = new FollowListRequest();
                    String str = followListRequest.StartWebClient();
                    if (str.Contains("7154326"))
                    {
                        Console.WriteLine("pass ok");
                        BattleSetupRequest setupRequest = new BattleSetupRequest();
                        for (int j = 0; j < num; j++)
                        {
                            setupRequest.StartWebClient(93000001, 3, BattleManager.activeDeckId, 7154326);
                            Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        Console.WriteLine("pass failed");
                        BattleSetupRequest setupRequest = new BattleSetupRequest();
                        for (int j = 0; j < num; j++)
                        {
                            setupRequest.StartWebClient(93000001, 3, BattleManager.activeDeckId, 0);
                            Thread.Sleep(1000);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("net work error" + e.ToString());
                    continue;
                }
            }
        }

    }
}
