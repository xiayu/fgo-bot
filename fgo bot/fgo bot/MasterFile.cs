﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace fgo_bot
{
    class MasterFile
    {
        protected static List<string> saveNameList = new List<string>();

        public static void WriteToPath(string path, string item)
        {
            StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8);
            sw.Write(item);
            sw.Close();
        }

        public static void DecryptFromPath(string path)
        {
            if (File.Exists(path))
            {
                using (BinaryReader binaryReader2 = new BinaryReader(File.OpenRead(path)))
                {
                    try
                    {
                        string str = binaryReader2.ReadString();
                        string item = CryptData.Decrypt(str);
                        WriteToPath(path + ".json", item);
                    }
                    catch (FileNotFoundException)
                    {
                    }
                }
            }
        }

        public static void ReadMasterData()
        {
            string cacheListFileName = @"MasterDataCaches\masterDataList.dat";
            if (File.Exists(cacheListFileName))
            {
                using (BinaryReader binaryReader = new BinaryReader(File.OpenRead(cacheListFileName)))
                {
                    try
                    {
                        string text = binaryReader.ReadString();
                        int dataVersion = binaryReader.ReadInt32();
                        int saveListSum = binaryReader.ReadInt32();
                        for (int i = 0; i < saveListSum; i++)
                        {
                            saveNameList.Add(binaryReader.ReadString());
                            DecryptFromPath(@"MasterDataCaches\" + saveNameList[i] + @".dat");
                        }
                    }
                    catch (FileNotFoundException)
                    {
                    }
                }
            }
        }
    }
}
