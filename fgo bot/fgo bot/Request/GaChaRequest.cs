﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request 
{
    class GaChaRequest : BaseRequest
    {

        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "gacha/draw";
        }

        public void TestAuth(int gachaId, int num, int warId, int ticketItemId, int shopIdIdx)
        {
            SortedDictionary<string, string> values = new SortedDictionary<string, string>();
            values.Add("gachaId", gachaId.ToString());
            values.Add("num", num.ToString());
            values.Add("ticketItemId", ticketItemId.ToString());
            values.Add("shopIdIndex", shopIdIdx.ToString());

            values.Add("userId", "561047");
            values.Add("authKey", "q10ZwyQriNGFF2pq:VxNWAAAAAAA=");
            values.Add("appVer", "1.0.3");
            values.Add("dataVer", "15");
            values.Add("lastAccessTime", "1438839093");
//            NetworkManager.secretKey = 
        }

        public string StartWebClient(int gachaId, int num, int ticketItemId, int shopIdIdx)
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Miscellaneous", "X-Unity-Version: 4.6.7p1");
                if (NetworkManager.cookie != null)
                    client.Headers.Add("Cookie", NetworkManager.cookie);
                SortedDictionary<string, string> ex = new SortedDictionary<string, string>();
                ex.Add("gachaId", gachaId.ToString());
                ex.Add("num", num.ToString());
                ex.Add("ticketItemId", ticketItemId.ToString());
                ex.Add("shopIdIndex", shopIdIdx.ToString());
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetField(ex));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                BattleManager.setSvt1(ResultAuthTicket);
                dumpToFile("gacha@draw", ResultAuthTicket);
                return ResultAuthTicket;
            }
        }
    }
}
