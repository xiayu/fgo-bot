﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;


namespace fgo_bot.Request
{
    class TopLoginRequest :BaseRequest
    {

        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "login/top?_userId=" + NetworkManager.userId;
        }

        private string getFreeStore(string str)
        {
            JObject jo = JObject.Parse(str);
            return jo["cache"]["replaced"]["userGame"][0]["freeStone"].ToString();
        }

        public String StartWebClient()
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                //client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("X-Unity-Version", "4.6.7p1");
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetBaseField(true));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("login@top", ResultAuthTicket);
                getFreeStore(ResultAuthTicket);
                return ResultAuthTicket;
            }  
        }
    }
}
