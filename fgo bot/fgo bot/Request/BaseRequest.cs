﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    abstract class BaseRequest
    {
        public static string UAHeader = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_1_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12B466";

        //public static string UAHeader = "Dalvik/1.6.0 (Linux; U; Android 4.4.2; GT-N7100 Build/KOT49H)";
        public abstract string GetUrl();

        public void dumpToFile(string name, string content)
        {
            if (Program.DebugMode)
            {
                using (StreamWriter sw = new StreamWriter(name + ".json", false, Encoding.UTF8))
                {
                    Console.WriteLine(content);
                    sw.Write(content);
                }
            }
        }
    }
}
