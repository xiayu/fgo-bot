﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class AccountRegistRequest : BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl(true) + "account/regist";
        }

        public String StartWebClient()
        {
            BattleManager.svtList.Clear();
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("X-Unity-Version", "4.6.7p1");
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetBaseField(true));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("account@regist", ResultAuthTicket);
                JsonToAuth(ResultAuthTicket);
                return ResultAuthTicket;
            }
        }

        private void JsonToAuth(string str)
        {
            JObject jo = JObject.Parse(str);
            JArray ja = jo["response"].Value<JArray>();
            jo = ja[0].Value<JObject>();
            jo = jo["success"].Value<JObject>();
            NetworkManager.authKey = jo["authKey"].ToString();
            NetworkManager.secretKey = jo["secretKey"].ToString();
            NetworkManager.userId = jo["userId"].ToString();
            NetworkManager.serverOffsetTime = 1438853566;
            NetworkManager.userCreateServer = "game.fate-go.jp/";
            NetworkManager.WriteAuth();
        }

        public void MockTest()
        {
            using (StreamReader sr = new StreamReader("account@regist.json"))
            {
                JsonToAuth(sr.ReadToEnd());
            }
        }
    }
}
