﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;


namespace fgo_bot.Request
{
    class PresentListRequest:BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "present/list?_userId=" + NetworkManager.userId;
        }

        public string StartWebClient()
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Miscellaneous", "X-Unity-Version: 4.6.7p1");
                if (NetworkManager.cookie != null)
                    client.Headers.Add("Cookie", NetworkManager.cookie);
                SortedDictionary<string, string> ex = new SortedDictionary<string, string>();
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetField(ex));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("present@list", ResultAuthTicket);
                return GetPresentList(ResultAuthTicket);
            }
        }

        private string GetPresentList(string str)
        {
            List<string> list = new List<string>();
            JObject jo = JObject.Parse(str);
            JArray ja = jo["cache"]["replaced"]["userPresentBox"].Value<JArray>();
            if (ja.Count == 0)
            {
                return null;
            }
            foreach (JObject item in ja)
            {
                list.Add(item["presentId"].ToString());
            }
            string temp = string.Join(",", list);
            temp = "[" + temp + "]";
            Console.WriteLine(temp);
            return temp ;
        }
    }
}
