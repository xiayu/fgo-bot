﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class DeckSetupRequest:BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "deck/setup?_userId=" + NetworkManager.userId;
        }

        public string getJson()
        {
            string p = "[{\"id\":" + BattleManager.activeDeckId + ",\"userId\":0,\"deckNo\":0,\"name\":\"\",\"cost\":7,\"deckInfo\":{\"svts\":[{\"id\":1,\"userSvtId\":" + BattleManager.svt + ",\"isFollowerSvt\":false},{\"id\":2,\"userSvtId\":" + BattleManager.svt1 + ",\"isFollowerSvt\":false},{\"id\":3,\"userSvtId\":0,\"isFollowerSvt\":true},{\"id\":4,\"userSvtId\":0,\"isFollowerSvt\":false},{\"id\":5,\"userSvtId\":0,\"isFollowerSvt\":false},{\"id\":6,\"userSvtId\":0,\"isFollowerSvt\":false}]}}]";
            return p;
        }

        public string StartWebClient(long mainDeckId, long activeDeckId)
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Miscellaneous", "X-Unity-Version: 4.6.7p1");
                if (NetworkManager.cookie != null)
                    client.Headers.Add("Cookie", NetworkManager.cookie);
                SortedDictionary<string, string> ex = new SortedDictionary<string, string>();
                ex.Add("mainDeckId", mainDeckId.ToString());
                ex.Add("activeDeckId", activeDeckId.ToString());
                ex.Add("userDeck", getJson());
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetField(ex));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("tutorial@set", ResultAuthTicket);
                //BattleManager.setActivrDeckId(ResultAuthTicket);
                return ResultAuthTicket;
            }
        }
    }
}
