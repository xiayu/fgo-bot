﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class BattleResultRequest : BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "battle/result?_userId=" + NetworkManager.userId;
        }

        public string StartWebClient(int battleId, int battleResult, string scores, string action)
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Miscellaneous", "X-Unity-Version: 4.6.7p1");
                if (NetworkManager.cookie != null)
                    client.Headers.Add("Cookie", NetworkManager.cookie);
                SortedDictionary<string, string> ex = new SortedDictionary<string, string>();
                ex.Add("battleId", battleId.ToString());
                ex.Add("battleResult", battleResult.ToString());
                ex.Add("scores", scores.ToString());
                ex.Add("action", action.ToString());
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetField(ex));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("battle@result", ResultAuthTicket);
                BattleManager.setFollowId(ResultAuthTicket);
                return ResultAuthTicket;
            }
        }
    }
}
