﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class TopSignUpRequest : BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "signup/top?_userId=" + NetworkManager.userId;
        }

        public string StartWebClient()
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("X-Unity-Version", "4.6.7p1");
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetBaseField(true));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                return ResultAuthTicket;
            }
        }
    }
}
