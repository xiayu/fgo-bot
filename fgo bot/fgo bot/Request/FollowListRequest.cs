﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class FollowListRequest :BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "follower/list?_userId=" + NetworkManager.userId; ;
        }

        public string StartWebClient()
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Miscellaneous", "X-Unity-Version: 4.6.7p1");
                if (NetworkManager.cookie != null)
                    client.Headers.Add("Cookie", NetworkManager.cookie);
                SortedDictionary<string, string> ex = new SortedDictionary<string, string>();
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetField(ex));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("follower@list", ResultAuthTicket);
                return ResultAuthTicket;
            }
        }
    }
}
