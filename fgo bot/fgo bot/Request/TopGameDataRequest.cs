﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class TopGameDataRequest : BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "gamedata/top?_userId="+NetworkManager.userId;
        }

        public String StartWebClient()
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("X-Unity-Version", "4.6.7p1");
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetBaseField(false));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("gamedata@top", ResultAuthTicket);
                return ResultAuthTicket;
            }
        }
    }
}
