﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class BattleSetupRequest :BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "battle/setup?_userId=" + NetworkManager.userId;
        }

        public string StartWebClient(int questId, int questPhase, long activeDeckId, long followerId)
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Miscellaneous", "X-Unity-Version: 4.6.7p1");
                if (NetworkManager.cookie != null)
                    client.Headers.Add("Cookie", NetworkManager.cookie);
                SortedDictionary<string, string> ex = new SortedDictionary<string, string>();
                ex.Add("questId", questId.ToString());
                ex.Add("questPhase", questPhase.ToString());
                ex.Add("activeDeckId", activeDeckId.ToString());
                ex.Add("followerId", followerId.ToString());
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetField(ex));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("battle@setup", ResultAuthTicket);
                BattleManager.setBattleIDByJson(ResultAuthTicket);
                BattleManager.setSvt(ResultAuthTicket);
                return ResultAuthTicket;
            }
        } 
    }
}
