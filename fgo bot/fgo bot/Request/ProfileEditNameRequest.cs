﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fgo_bot.Request
{
    class ProfileEditNameRequest:BaseRequest
    {
        public override string GetUrl()
        {
            return NetworkManager.GetServerUrl() + "profile/editName?_userId=" + NetworkManager.userId;
        }

        public string StartWebClient(string changeName, int changeType)
        {
            using (GzipWebClient client = new GzipWebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("User-Agent", UAHeader);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                if (NetworkManager.cookie != null)
                    client.Headers.Add("Cookie", NetworkManager.cookie);
                client.Headers.Add("Miscellaneous", "X-Unity-Version: 4.6.7p1");
                SortedDictionary<string, string> ex = new SortedDictionary<string, string>();
                ex.Add("name", changeName.ToString());
                ex.Add("genderType", changeType.ToString());
                byte[] result = client.UploadValues(GetUrl(), "POST", NetworkManager.SetField(ex));
                string ResultAuthTicket = Encoding.UTF8.GetString(result);
                dumpToFile("profile@editName", ResultAuthTicket);
                return ResultAuthTicket;
            }
        }
    }
}
