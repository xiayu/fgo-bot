__author__ = 'xiayu'

import web

urls = (
    '/hello', 'drop',
    '/index', 'index'
)

db = web.database(dbn='mysql', user='root', pw='', db='fgo_bot')

class index:
    def GET(self):
        return 'index success'

class drop:
    def GET(self):
        userid = web.input()
        q = db.insert('dropitemobject', itemNum=userid.itemNum, itemList=userid.itemList, questId=userid.questId)
        return 'success'

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()

